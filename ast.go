package goast

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/dave/dst"
	"github.com/dave/dst/decorator"
	"github.com/dave/dst/decorator/resolver/guess"
	"go/format"
	"go/parser"
	"go/token"
	"os"
	"regexp"
	"strings"
)

func WriteASTToFile(astFile *dst.File, filepath string) error {
	var err error
	var outputFile *os.File
	outputFile, err = os.Create(filepath)
	defer outputFile.Close()

	fset := token.NewFileSet()
	err = format.Node(outputFile, fset, astFile)
	if err != nil {
		return fmt.Errorf("could not write to file: %v", err)
	}

	return nil
}

func PrintAST(file *dst.File) (string, error) {
	var buf bytes.Buffer
	res := decorator.NewRestorerWithImports("main", guess.New())
	if err := res.Fprint(&buf, file); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func ComposeTags(tags []Tag) string {
	var buf bytes.Buffer
	buf.WriteString("`")
	for _, tag := range tags {
		buf.WriteString(tag.Name)
		buf.WriteString(":")
		buf.WriteString(fmt.Sprintf("\"%s\"", tag.Value))
		buf.WriteString(" ")
	}
	buf.WriteString("`")
	return strings.TrimSpace(buf.String())
}

func GetASTFromFile(filename string) (*token.FileSet, *dst.File, error) {
	fset := token.NewFileSet()
	file, err := decorator.ParseFile(fset, filename, nil, parser.ParseComments)
	if err != nil {
		return nil, nil, err
	}

	return fset, file, nil
}

type Struct struct {
	Name     string
	Fields   []Field
	Methods  []Method
	Comments []Comment
}

type Comment struct {
	Text  string
	Slash token.Pos
}

type Method struct {
	Name         string
	Receiver     string
	ReceiverType string
	Arguments    []Param
	Return       []Param
	Body         *dst.BlockStmt
}

type Param struct {
	Name      string
	Type      string
	IsPointer bool
}

type Field struct {
	Name      string
	Tags      []Tag
	IsPointer bool
	Type      string
}

type Tag struct {
	Name  string
	Value string
}

func SyncStructs(file *dst.File, structs map[string]*Struct) error {
	for _, decl := range file.Decls {
		genDecl, ok := decl.(*dst.GenDecl)
		if ok && genDecl.Tok == token.TYPE {
			for _, spec := range genDecl.Specs {
				typeSpec, ok := spec.(*dst.TypeSpec)
				if ok && typeSpec.Type != nil {
					structType, ok := typeSpec.Type.(*dst.StructType)
					if ok {
						s, ok := structs[typeSpec.Name.Name]
						if !ok {
							continue
						}

						structType.Fields.List = make([]*dst.Field, 0, len(s.Fields))
						for i, field := range s.Fields {
							var fieldType dst.Expr
							if field.IsPointer {
								fieldType = &dst.StarExpr{
									X: &dst.Ident{
										Name: field.Type,
									},
								}
							} else {
								fieldType = &dst.Ident{
									Name: field.Type,
								}
							}
							astField := &dst.Field{
								Names: []*dst.Ident{
									{
										Name: field.Name,
									},
								},
								Type: fieldType,
							}
							astField.Tag = &dst.BasicLit{
								Kind:  token.STRING,
								Value: ComposeTags(s.Fields[i].Tags),
							}
							structType.Fields.List = append(structType.Fields.List, astField)
						}
					}
				}
			}
		}
	}
	return nil
}

func getStructs(file *dst.File) map[string]*Struct {
	structs := make(map[string]*Struct, 0)
	var skipStructs []string

	for _, decl := range file.Decls {
		genDecl, ok := decl.(*dst.GenDecl)
		if ok && genDecl.Tok == token.TYPE {
			for _, spec := range genDecl.Specs {
				typeSpec, ok := spec.(*dst.TypeSpec)
				if ok && typeSpec.Type != nil {
					structType, ok := typeSpec.Type.(*dst.StructType)
					if ok {
						s := Struct{
							Name: typeSpec.Name.Name,
						}
						for _, field := range structType.Fields.List {
							if len(field.Names) > 0 {
								f := Field{
									Name: field.Names[0].Name,
								}
								if field.Tag != nil {
									f.Tags = parseTag(field.Tag.Value)
								}
								switch field.Type.(type) {
								case *dst.StarExpr:
									f.IsPointer = true
									f.Type = field.Type.(*dst.StarExpr).X.(*dst.Ident).Name
								case *dst.Ident:
									f.Type = field.Type.(*dst.Ident).Name
								}
								s.Fields = append(s.Fields, f)
							}
						}
						structs[s.Name] = &s
					}
				}
			}
		}
	}
	for _, s := range skipStructs {
		delete(structs, s)
	}

	return structs
}

func removeEmptyLines(lines []string) []string {
	var result []string
	for _, line := range lines {
		if line != "" {
			result = append(result, line)
		}
	}
	return result
}

func GetStructs(file *dst.File) map[string]*Struct {
	structs := getStructs(file)
	methods := getMethods(file)
	for _, s := range structs {
		s.Methods = methods[s.Name]
	}
	return structs
}

func getMethods(file *dst.File) map[string][]Method {

	methods := make(map[string][]Method, 0)

	for _, decl := range file.Decls {
		funcDecl, ok := decl.(*dst.FuncDecl)
		if ok && funcDecl.Recv != nil && len(funcDecl.Recv.List) > 0 {
			// Получаем тип получателя метода
			var typeName string
			switch t := funcDecl.Recv.List[0].Type.(type) {
			case *dst.StarExpr:
				typeName = t.X.(*dst.Ident).Name
			case *dst.Ident:
				typeName = t.Name
			}

			// Получаем имя переменной получателя
			var receiverName string
			if len(funcDecl.Recv.List[0].Names) > 0 {
				receiverName = funcDecl.Recv.List[0].Names[0].Name
			}

			structName := strings.ReplaceAll(typeName, "*", "")

			params := make([]Param, 0)
			if funcDecl.Type.Params != nil {
				for _, param := range funcDecl.Type.Params.List {
					for _, name := range param.Names {
						if _, ok := param.Type.(*dst.StarExpr); ok {
							params = append(params, Param{
								Name:      name.Name,
								Type:      param.Type.(*dst.StarExpr).X.(*dst.Ident).Name,
								IsPointer: true,
							})
							continue
						}
						params = append(params, Param{
							Name: name.Name,
							Type: param.Type.(*dst.Ident).Name,
						})
					}
				}
			}

			returnParams := make([]Param, 0)
			if funcDecl.Type.Results != nil {
				for _, field := range funcDecl.Type.Results.List {
					// для простоты, мы предполагаем, что у нас только один возвращаемый тип для этой функции
					switch fieldType := field.Type.(type) {
					case *dst.Ident:
						returnParams = append(returnParams, Param{
							Name: "",
							Type: fieldType.Name,
						})
					case *dst.StarExpr:
						returnParams = append(returnParams, Param{
							Name:      "",
							Type:      fieldType.X.(*dst.Ident).Name,
							IsPointer: true,
						})
					case *dst.MapType:
						var key string
						if _, ok := fieldType.Value.(*dst.InterfaceType); ok {
							key = "interface{}"
						}
						var value string
						switch t := fieldType.Value.(type) {
						case *dst.Ident:
							value = t.Name
						case *dst.StarExpr:
							value = t.X.(*dst.Ident).Name
						case *dst.InterfaceType:
							value = "interface{}"
						}
						returnParams = append(returnParams, Param{
							Name: "",
							Type: fmt.Sprintf("map[%s]%s", key, value),
						})
					default:
						fmt.Println(fieldType)
					}
				}
			}

			methods[structName] = append(methods[typeName], Method{
				Name:         funcDecl.Name.Name,
				Receiver:     receiverName,
				ReceiverType: typeName,
				Arguments:    params,
				Return:       returnParams,
			})
		}
	}

	return methods
}

func parseTag(fieldTag string) []Tag {
	rawTags := strings.Split(fieldTag, "\" ")
	tags := make([]Tag, 0)
	for _, rawTag := range rawTags {
		tag := Tag{}
		tag.Name = strings.Split(rawTag, ":")[0]
		tag.Name = strings.ReplaceAll(tag.Name, "`", "")
		tag.Name = strings.TrimSpace(tag.Name)
		value := strings.Split(rawTag, ":")[1]
		tag.Value = strings.ReplaceAll(value, "\"", "")
		tag.Value = strings.ReplaceAll(tag.Value, "`", "")
		tag.Value = strings.TrimSpace(tag.Value)
		tags = append(tags, tag)
	}

	return tags
}

func AddMethod(file *dst.File, structName string, method Method) (string, error) {
	structs := GetStructs(file)
	s := structs[structName]
	for _, m := range s.Methods {
		if m.Name == method.Name {
			return "", errors.New("method already exists")
		}
	}

	var structIndex int = -1
	// Находим узел структуры и его индекс
	for idx, decl := range file.Decls {
		if _, ok := decl.(*dst.GenDecl); ok {
			spec, ok := decl.(*dst.GenDecl).Specs[0].(*dst.TypeSpec)
			if ok && spec.Name.Name == "User" {
				structIndex = idx
				break
			}
		}
	}

	if structIndex == -1 {
		return "", errors.New("struct not found")
	}

	// Создаем новый метод для структуры User
	newMethod := &dst.FuncDecl{
		Name: dst.NewIdent(method.Name),
		Recv: &dst.FieldList{
			List: []*dst.Field{
				{
					Names: []*dst.Ident{
						dst.NewIdent(method.Receiver),
					},
					Type: &dst.StarExpr{X: dst.NewIdent(method.ReceiverType)},
				},
			},
		},
		Body: method.Body,
	}

	// Добавляем аргументы
	for _, arg := range method.Arguments {
		if newMethod.Type == nil {
			newMethod.Type = &dst.FuncType{}
		}
		if newMethod.Type.Results == nil {
			newMethod.Type.Results = &dst.FieldList{}
		}
		newMethod.Type.Params.List = append(newMethod.Type.Params.List, &dst.Field{
			Names: []*dst.Ident{
				dst.NewIdent(arg.Name),
			},
			Type: dst.NewIdent(arg.Type),
		})
	}

	// Добавляем возвращаемые значения
	for _, ret := range method.Return {
		if newMethod.Type == nil {
			newMethod.Type = &dst.FuncType{}
		}
		if newMethod.Type.Results == nil {
			newMethod.Type.Results = &dst.FieldList{}
		}
		newMethod.Type.Results.List = append(newMethod.Type.Results.List, &dst.Field{
			Type: dst.NewIdent(ret.Type),
		})
	}

	// Вставляем новый метод сразу после структуры
	file.Decls = append(file.Decls, newMethod)
	file.Decls[len(file.Decls)-1], file.Decls[structIndex+1] = file.Decls[structIndex+1], file.Decls[len(file.Decls)-1]

	// Генерируем новый код
	var buf bytes.Buffer
	res := decorator.NewRestorerWithImports("main", guess.New())
	if err := res.Fprint(&buf, file); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func ModifyStructs(structs map[string]*Struct, ms ...ModifyStruct) error {
	var err error
	for _, s := range structs {
		for _, m := range ms {
			err = m(s)
		}
	}

	return err
}

type ModifyStruct func(s *Struct) error

func AddDBTags(s *Struct) error {
	var tag Tag
	for i := range s.Fields {
		var found bool
		for j := range s.Fields[i].Tags {
			if s.Fields[i].Tags[j].Name == "db" {
				found = true
			}
		}
		if found {
			continue
		}

		tag.Name = "db"
		tag.Value = camelToSnake(s.Fields[i].Name)
		s.Fields[i].Tags = append(s.Fields[i].Tags, tag)
	}

	return nil
}

func AddDBTypeTags(s *Struct) error {
	var tag Tag
	for i := range s.Fields {
		var found bool
		for _, originTag := range s.Fields[i].Tags {
			if originTag.Name == "db_type" {
				found = true
			}
		}
		if found {
			continue
		}

		tag.Name = "db_type"
		tag.Value = getDbType(s.Fields[i].Type)
		s.Fields[i].Tags = append(s.Fields[i].Tags, tag)
	}

	return nil
}

func getDbType(fieldType string) string {
	switch fieldType {
	case "int":
		return "INT"
	case "string":
		return "VARCHAR(255)"
	case "float64":
		return "DECIMAL(21,8)"
	default:
		return fieldType
	}
}

func camelToSnake(s string) string {
	// Разделяем на подстроки, предварительно заменяя заглавные буквы
	if len(s) == 2 {
		return strings.ToLower(s)
	}
	var re = regexp.MustCompile(`([A-Z])`)
	snake := re.ReplaceAllString(s, `_$1`)

	return strings.TrimPrefix(strings.ToLower(snake), "_")
}
